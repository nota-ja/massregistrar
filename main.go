package main

import (
        "flag"
        "log"
        "math/rand"
        "os"
        "strconv"
        "strings"
        "time"

        "github.com/cloudfoundry/gibson"
        "github.com/cloudfoundry/yagnats"
)

const (
        INT24MAX = 16777216
        INT16MAX = 65536
        INT08MAX = 256
)

var numDea = flag.Int("numDea", 1, "Number of dummy DEAs to publish 'router.register'")
var insPerDea = flag.Int("insPerDea", 1, "Number of dummy app instances on a dummy DEA")
var numUrl = flag.Int("numUrl", 1, "Number of dummy app URLs")
var domain = flag.String("domain", ".vcap.me", "Base domain used for dummy URLs (must starts with '.')")
var writeUrl = flag.Bool("writeUrl", false, "True if write dummy URLs to file './urls.*.txt'")

var natsAddresses = flag.String("natsAddresses", "", "comma-separated list of NATS cluster member IP:ports")
var natsUsername = flag.String("natsUsername", "", "authentication user for connecting to NATS")
var natsPassword = flag.String("natsPassword", "", "authentication password for connecting to NATS")

func main() {
        flag.Parse()
        if (*numDea <= 0) || (*numDea > 1000) {
                log.Fatalln("Number of dummy DEAs must be between 1 .. 1000")
        }
        if (*insPerDea <= 0) || (*insPerDea > 500) {
                log.Fatalln("Number of dummy app instances per DEA must between 1 .. 100")
        }
        if (*numUrl <= 0) || (*numUrl > (*insPerDea * *numDea)) {
                log.Fatalln("Number of dummy URLs must between 1 .. number of instances")
        }

        for index := 1; index <= *numDea; index++ {
                go register(index)
                time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
        }

        select {}
}

func intToIpv4Addr(val int) string {
        leftOctet := strconv.Itoa(val / INT16MAX)
        middleOctet := strconv.Itoa((val % INT16MAX) / INT08MAX)
        rightOctet := strconv.Itoa((val % INT16MAX) % INT08MAX)

        return strings.Join([]string{"127", leftOctet, middleOctet, rightOctet}, ".")
}

func register(index int) {
        ip := intToIpv4Addr(index)

        urls := make(map[string]bool)
        routeMap := make(map[int]string)
        for ii := 0; ii < *insPerDea; ii++ {
                port := 30000 + ii
                url := "u" + strconv.Itoa(rand.Intn(*numUrl)) + *domain
                routeMap[port] = url
                urls[url] = true
        }
        if *writeUrl {
                filename := "./urls." + strconv.Itoa(index) + ".txt"
                file, err := os.Create(filename)
                if err == nil {
                        for url := range urls {
                                _, errOnWrite := file.WriteString("http://" + url + "/\n")
                                if errOnWrite != nil {
                                        file.Close()
                                        log.Panicln(err)
                                        break
                                }
                        }
                } else {
                        log.Panicln(err)
                }
        }

        nats := yagnats.NewClient()

        natsMembers := []yagnats.ConnectionProvider{}

        if *natsAddresses == "" {
                log.Fatalln("must specify at least one nats address (-natsAddresses=1.2.3.4:5678)")
        }

        for _, addr := range strings.Split(*natsAddresses, ",") {
                log.Println("configuring nats server:", addr)
                natsMembers = append(natsMembers, &yagnats.ConnectionInfo{
                        Addr:     addr,
                        Username: *natsUsername,
                        Password: *natsPassword,
                })
        }

        if len(natsMembers) == 0 {
                log.Fatalln("must specify at least one nats address")
        }

        natsInfo := &yagnats.ConnectionCluster{natsMembers}

        for {
                err := nats.Connect(natsInfo)
                if err == nil {
                        break
                }

                log.Println("failed to connect to NATS:", err)
                time.Sleep(1 * time.Second)
        }

        client := gibson.NewCFRouterClient(ip, nats)

        for {
                for port, url := range routeMap {
                        client.Register(port, url)
                        // log.Println("registered ", port, ":", url)
                }
                time.Sleep(30 * time.Second)
        }
}
